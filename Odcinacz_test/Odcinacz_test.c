/*
 * Odcinacz_test.c
 *
 * Created: 2017-02-06 21:04:01
 *  Author: Dan
 *
 *	Wersja kodu: 24.02.2017
 */ 

#define F_CPU 128000 // 128kHz - 0.14mA idle; 0.85mA praca (zwi�kszyc pullupy na zworkach? obecnie 4k7)

#define WYMUSZENIE_ODCINANIA_OFF PINB & _BV(1)
#define WYMUSZENIE_ODCINANIA_ON !(PINB & _BV(1))

#define ZWORA_OFF PINB & _BV(3)
#define ZWORA_ON !(PINB & _BV(3))
#define TRANZYSTOR_OFF PORTB &= ~_BV(4);
#define TRANZYSTOR_ON PORTB |= _BV(4);

//------------------------------------------------------------------
#define CZAS_ODCINANIA 1800  // 5400s to 1h 50min // 1800=30min

#define CZAS_BACKUPU 120    // 300s to 5min
//------------------------------------------------------------------

#define ADR_H 0x01
#define ADR_L ADR_H + 1

#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

void EEPROM_write (unsigned char ucAddress, unsigned char ucData);
unsigned char EEPROM_read (unsigned char ucAddress);

volatile uint16_t prog_timer_sek = 0;
volatile uint16_t prog_timer_sek_eeprom_backup = 0;
volatile uint8_t timer_wymuszenia = 0;

volatile uint8_t STARTUP_ZMIENNA = 0; // zmienna dzieki ktorej odczyt z eeprom zrobi tylko na starcie -- '0' znaczy ze byl restart

void init()
{
	DDRB = 0b00110101; // PB3 - zwora gorna [uzbrajanie];  PB1 - zwora dolna [wymuszenie odciecia]
	PORTB = 0b00000000;
	
	// TIMER2 W TRYBIE CTC
	OCR0A = 125;  // 128'000/1024 = 125 >> 125/125 = 1Hz
	TCCR0A |= (1<<WGM01); //tryb ctc
	TCCR0B |= (1<<CS02) | (1<<CS00);    // preskaler TIMER0 1024
	TIMSK0 |= (1<<OCIE0A);

	sei(); // globalne wlaczenie przerwan (cli() - wy��cza)
	
	wdt_enable(WDTO_2S);
}

ISR (TIM0_COMPA_vect) // obsluga przerwania od TIMER2
{
	prog_timer_sek++;
	prog_timer_sek_eeprom_backup++;
	timer_wymuszenia++;
}

//************************************************************************* MAIN CODE ************************************************************

int main(void)
{
	init();
	
	while (1)
	{			
				
		while( ZWORA_OFF ) // odcinacz rozbrojony: liczenie nie pracuje, zmienne i EEPROM wyczyszczone // 
		{
			wdt_reset();
			
			TRANZYSTOR_OFF;
			prog_timer_sek = 0;
			prog_timer_sek_eeprom_backup = 0;
			timer_wymuszenia = 0;
			
			if (EEPROM_read(ADR_H) != 0x00 || EEPROM_read(ADR_L) != 0x00) // 'wyczysc' eeprom jesli jest tam inna wartosc niz 0 [0x00]
			{
				EEPROM_write(ADR_H, 0x00);
				EEPROM_write(ADR_L, 0x00);
			}
			
			_delay_ms(1000);
		}
		
		
		while ( ZWORA_ON ) // odcinacz uzbrojony: normalna praca, liczy czas i backup'uje do EEPROM; >>>>>>> [liczymy do przodu ++] <<<<<<<<<
		{
			wdt_reset();
			
			// tu funkcja wczytywania z eeprom pozostalego czasu
			
			if (STARTUP_ZMIENNA == 0x00)
			{
				if (  (EEPROM_read(ADR_H) != 0x00 || EEPROM_read(ADR_L) != 0x00)    &&    (EEPROM_read(ADR_H) != 0xFF || EEPROM_read(ADR_L) != 0xFF)  )
				{
					cli();
					uint16_t tmp_H = EEPROM_read(ADR_H);
					uint16_t tmp_L = EEPROM_read(ADR_L);
					tmp_H = tmp_H << 8;
					prog_timer_sek = tmp_H + tmp_L;
					STARTUP_ZMIENNA = 0xFF;
					sei();
				}
				else if (EEPROM_read(ADR_H) == 0x00 && EEPROM_read(ADR_L) == 0x00)
				{ }
			}		
					
			//---------------------------------------------------------------------------------------------|
			
			if ( (prog_timer_sek >= CZAS_ODCINANIA && prog_timer_sek < CZAS_ODCINANIA+10) || prog_timer_sek >= CZAS_ODCINANIA+20 ) // odetnij linke gdy minal czas
			{
				TRANZYSTOR_ON;
			}
			else TRANZYSTOR_OFF;
			
			//---------------------------------------------------------------------------------------------|
						
			if (prog_timer_sek_eeprom_backup >= CZAS_BACKUPU ) // backup licznika do pamieci EEPROM
			{			
				uint8_t tmp2_H = (prog_timer_sek >> 8);
				uint8_t tmp2_L = (uint8_t)prog_timer_sek;
				EEPROM_write(ADR_H, tmp2_H);
				EEPROM_write(ADR_L, tmp2_L);
				prog_timer_sek_eeprom_backup = 0;
			}
			
			//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! PRZEPALANIE MANUALNE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 			
			
			timer_wymuszenia = 0;
			while (WYMUSZENIE_ODCINANIA_ON)
			{
				wdt_reset();
				
				if ( (timer_wymuszenia >= 10 && timer_wymuszenia < 20) || timer_wymuszenia >= 30 || prog_timer_sek >= CZAS_ODCINANIA )
				{
					TRANZYSTOR_ON;
					if (ZWORA_OFF) break; // naprawia problem zatrzasniecia w petli [ignorowano zwore uzbrajajaca przy wymuszeniu manualnym] ---- wywali� gdy manual ma byc nadrzedny i bedzie dobra lacznosc
				}
				else TRANZYSTOR_OFF;
			}
			
			//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!	
					
		}	
	}	
}

	#define EEPE 1
	#define EEMPE 2

void EEPROM_write (unsigned char ucAddress, unsigned char ucData)
{	
	/* Wait for completion of previous write*/
	while(EECR & (1<<EEPE));
	/* Set Programming mode */
	EECR &= ~(1<<EEPM1)|(1<<EEPM0);
	/* Set up address and data registers */
	EEARL = ucAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}

unsigned char EEPROM_read (unsigned char ucAddress)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE));
	/* Set up address register */
	EEARL = ucAddress;
	/* Start eeprom read by writing EERE */
	EECR |= (1<<EERE);
	/* Return data from data register */
	return EEDR;
}


