Firmware odcinacza linki. 

Po włożeniu zwory uzbrajającej zaczyna liczyć zaprogramowany na sztywno czas. Okresowo zapisywany jest stan licznika do pamięci nieulotnej (na wypadek resetu urządzenia, czyli liczenia od nowa). 
Po doliczeniu do określonej wartości włączany jest tranzystor w celu przepalenia linki drutem oporowym.
Po tym, następuje wyrwanie zwory uzbrajającej - należy przymocować ją do górnej części przepalanej linki. W przypadku niepowodzenia (niewyrwania), przepalanie przerywane jest po ustalonym czasie.

Język C, środowisko Atmel Studio 6.x, mikrokontroler Attiny13.

-- Wszystkie materiały udostępniane są na licencji open software/hardware do użytku niekomercyjnego

-- Uwaga - kompilacja w Atmel Studio 7+ uniemożliwi ponowne przeniesienie projektu do starszej wersji.

Może też wystąpić błąd z funkcją init();. Należy używać jej wtedy jako INLINE.

------------------------------------------------

Schematy, pliki PCB i dokumentacja (częściowo nieaktualna):

https://bitbucket.org/Dnuk/gondolav2-hardware-repo/overview

-------------------------------------------------------------

Kontakt do nas oraz więcej materiałów tutaj: https://www.facebook.com/DNFsystems/
